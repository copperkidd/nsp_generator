# NSP Generator
Bash script used to generate .nsp files (Linux)

## Dependencies:
* curl
* unzip
* wget
* python3
* python3-pip
* python3-requests
* python3-tqdm
* python3-unidecode

You can install these dependencies on Debian/Ubuntu using the following commands:

```sudo apt-get install curl python3 python3-pip unzip wget; pip3 install requests tqdm unidecode```

## Usage:
Before running the script for the first time, you may change the temporary working directory path by editing the ```$temp_dir``` variable in the config file.

Each time you run the script, it will automatically check for the latest version of CDNSP_Next and update if necessary.  Then, you can either a) choose a file from a numbered list, b) manually enter a title id, c) do a keyword search to find a specific file, or d) choose files to batch download.

Once you choose an option, you will be given another choice between a) Base game only, b) Base game + Latest Update, or c) Base game + Latest Update + All DLC.

The .nsp files will be stored in ```~/.nsp_generator/cdnsp/CDNSP_Next/_NSPOUT``` unless you specify a different path by setting ```$output_dir``` in ```config``` before running the script.

(Thanks to SciresM, rkk, panda, AnalogMan, MLEM and Dimtree)