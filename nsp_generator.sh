#!/usr/bin/env bash

printf "\nCopperkidd's NSP Generator for Linux\n"
printf "____________________________________\n\n"

# Set $git_dir variable
git_dir="$(dirname "$0")"

# Import files
source "$git_dir/config"
source "$git_dir/functions"

# Check for existence of custom output directory
if [ ! -z "$output_dir" ] && [ ! -d "$output_dir" ]
	then
		printf "A custom nsp output directory has been set in the config file but the directory does not exist. Please create the directory or remove the path from the config file and re-run the script\n\n"
		exit 0
fi

# Check to see if the script has been run before
if [[ -f "$temp_dir"/firstrun ]]
	then
		versionCheck
	else
		firstRun
fi

updateTitlekeys

makeSelection

exit 0